using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ControllerServer : MonoBehaviour
{
	private InputController input;
	Dictionary<string, int> players = new Dictionary<string, int>();
	private bool[] openPlayers = new bool[4];
	private bool gameStarted = false;
	private float timeStarted;
	private int winner = 5;
	private float startTime;
	private float gameLength = 30;

	private PointBox pointBox;
	
	void Start() {

		pointBox = GameObject.Find("PointBox").GetComponent<PointBox>();

		input = InputController.Singleton;
		NetworkConnectionError error = Network.InitializeServer(4, 3030, false);
		Debug.Log("Started... " + error);
		for(int i=0; i < 4; i++){
			openPlayers[i] = true;
		}
		startTime = Time.time;
	}

	void Update() {
		if (!gameStarted) {
			tryStartGame ();
		} else if (Time.time - timeStarted >= gameLength){
			endGame();
		}
	}

	void endGame() {
		gameStarted = false;
		winner = input.findWinner ();
		pointBox.ResetPosition();
	}

	void tryStartGame() {
		bool hasAtLeastOneCat = false;
		bool allCatsInBox = true;
		for(int i = 0; i < 4; i++) {
			if (!openPlayers[i]) {
				hasAtLeastOneCat = true;
				if(!input.isCatInBox (i)) { 
					allCatsInBox = false;
				}
			}
		}
		if (hasAtLeastOneCat && allCatsInBox) {	
			input.startGame();
			winner = 5;
			gameStarted = true;
			timeStarted = Time.time;
		}
	}

	void OnGUI () {
		if (gameStarted) {
			input.showScores (Time.time - timeStarted, gameLength);
		} else if(Time.time - startTime > 5) {
			float widthUnit = Screen.width / 32f;
			float heightUnit = Screen.height / 18f;

			GUIStyle textStyle = new GUIStyle(GUI.skin.textField);
			textStyle.alignment = TextAnchor.MiddleCenter;
			textStyle.fontSize=20;

			GUI.Label(new Rect(widthUnit * 1, heightUnit * 3, widthUnit * 6, heightUnit * 2), "Get that laser pointer!!\nYOU WANT IT!!!", textStyle);
			GUI.Label(new Rect(widthUnit * 19, heightUnit * 1, widthUnit * 12, heightUnit * 2), "Rub the carpet for SUPER STATIC CHARGE!\nBlast away your enemies!!!",textStyle);

			GUI.Label(new Rect (widthUnit * 11, heightUnit * 6, widthUnit * 10, heightUnit * 1), "Move all the cats to the center to start.", textStyle);
			if (winner != 5) {
				GUI.Label (new Rect (widthUnit * 13, heightUnit * 3, widthUnit * 6, heightUnit * 1), "PLAYER " + (winner + 1).ToString() + " WINS!!!!", textStyle);
			}
		}
	}

	void OnPlayerConnected(NetworkPlayer player) {
		if (!gameStarted) {
			MovementScript newCat = null;
			for (int i = 0; i < 4; i++) {
				if (openPlayers [i]) {
					openPlayers [i] = false;
					players.Add (player.ToString (), i);
					Debug.Log ("HI player " + i);
					newCat = input.addPlayer (i);
					break;
				}
			}
			if(!newCat) {
				Network.CloseConnection(player, false);
			} else {
				Color newColor = newCat.getColor();
				Vector3 color = new Vector3(newColor.r, newColor.g, newColor.b);
				networkView.RPC("ChangeColor", player, new object[]{color});
			}
		} else {
			Network.CloseConnection(player, false);
		}
	}
	
	void OnPlayerDisconnected(NetworkPlayer player)
	{
		Debug.Log ("player left" + players[player.ToString()]);
		openPlayers[players[player.ToString()]] = true;
		input.removePlayer (players[player.ToString()]);
	} 

	void OnApplicationQuit() {
		Network.Disconnect();
	}
	
	
	[RPC]
	void Ping() {
		Debug.Log ("PING @" + Time.time);
	}

	[RPC]
	void ChangeColor(Vector3 color, NetworkMessageInfo info) {
		Debug.Log("ChangeColor(" + color + ") @" + Time.time);
	}
	
	[RPC]
	void ReceiveHop(Vector3 hopDir, float magnitude, NetworkMessageInfo info) {
		input.handleHop (hopDir, magnitude, players[info.sender.ToString ()]);
	}

	[RPC]
	void ReceiveCharge(float magnitude, NetworkMessageInfo info) {
		//Debug.Log("ReceiveCharge(" + magnitude + ") @" + Time.time);
		input.handleCharge (magnitude, players[info.sender.ToString ()]);
	}
}

