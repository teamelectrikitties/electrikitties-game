﻿using UnityEngine;
using System.Collections;

public class ElectricBallScript : MonoBehaviour {
	float hitCount = 0;
	// Use this for initialization
	void Start () {
		Destroy (gameObject, 5);
	}
	
	void Update() {
		transform.position = this.transform.parent.transform.position;
	}
	
	void OnTriggerEnter2D(Collider2D collision) {
		// If collides into a player, the target gets hit
		hitCount++;
		if (collision.tag == "Player" && hitCount > 1) {
			collision.gameObject.GetComponent<MovementScript>().hit(transform.position.x, transform.position.y);
			Destroy(this.gameObject);
		}
	}
}
