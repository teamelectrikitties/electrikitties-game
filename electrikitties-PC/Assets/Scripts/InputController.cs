﻿using UnityEngine;
using System.Collections;

public class InputController : ScriptableObject {
	
	private MovementScript[] cats = new MovementScript[4];
	private GameObject[] temps = new GameObject[4];
	private bool gameStarted = false;

	private static InputController singleton = null;
	public static InputController Singleton 
	{
		get  
		{
			if(singleton == null) {
				singleton = (InputController)ScriptableObject.CreateInstance("InputController");
			}
			return singleton;
		}
	}


	// Use this for initialization
	void Start () {
	}

	public bool isCatInBox(int id){
		return cats [id].isInBox ();
	}

	public int findWinner() {
		int winner = 5;
		int highScore = 0;
		for (int i=0;i<4;i++){
			if (cats[i] != null) {
				if (int.Parse(cats[i].getScore()) > highScore) {
					highScore = int.Parse(cats[i].getScore());
					winner = i;
				}
				cats[i].endGame();
			}
		}
		gameStarted = false;
		return winner;
	}

	public void startGame(){
		gameStarted = true;
		for (int i=0; i<4; i++) {
			if (cats[i] != null) {
				cats[i].startGame();	
			}
		}
	}

	public void showScores (float time, float length) {
		if (gameStarted) {
			float widthUnit = Screen.width / 4f;
			float heightUnit = Screen.height / 9f;
			GUIStyle textStyle = new GUIStyle(GUI.skin.textField);
			textStyle.fontSize=25;
			GUI.TextField (new Rect (widthUnit * 2 - 50, 0, 100, 35), (length -time).ToString(), 5, textStyle);
			textStyle = new GUIStyle(GUI.skin.textField);
			textStyle.fontSize=15;
			for (int i=0; i<4; i++) {
				if (cats [i] != null) {
					GUI.TextField (new Rect (widthUnit * i, heightUnit, 100, 25), cats [i].getScore (), 5, textStyle);
				}
			}
		}
	}
	void Update() {	
	}


	public bool hasStarted() {
		return gameStarted;
	}

	public MovementScript addPlayer(int id) {
		Object prefab = Resources.Load<GameObject>("Kitty");
		Vector3 start = Vector3.zero;
		Color color = Color.white;
		if (id == 0) {
			start.x = 50;
			start.y = -50;
			// Brown
			color = new Color(.4f,.3f,.2f,1f);
		}
		if (id == 1) {
			start.x = -50;
			start.y = -50;
			// Grey
			color = new Color(.5f,.5f,.5f,1f);
		}
		if (id == 2) {
			start.x = 50;
			start.y = 50;
			// Orange
			color = new Color(1f,.8f,.4f,1f);
		}
		if (id == 3) {
			start.x = -50;
			start.y = 50;
			// White
			color = new Color(1f,1f,1f,1f);
		}
		temps[id] = Instantiate(prefab, start, Quaternion.identity) as GameObject;
		cats [id] = (MovementScript)temps[id].GetComponent ("MovementScript");
		cats [id].setStart (start.x, start.y);
		cats [id].setColor (color);

		return cats[id];
	}

	public void removePlayer(int id){
		cats [id].remove ();
	}

	public void handleHop (Vector3 hopDir, float magnitude, int player){
		cats [player].makeMove (hopDir, magnitude);
	}

	public void handleCharge(float magnitude, int player) {
		cats [player].charge (magnitude);

	}

}
