using UnityEngine;
using System.Collections;

public class MovementScript : MonoBehaviour {
	
	
	public float angle = 0;	
	private float speedRev = 0;
	private bool jumping;
	private Vector3 tempHopDir;
	private float tempMagnitude;
	private float tempTime;
	private float startX;
	private float startY;
	private float lagTime = 0.75f;
	private float edgeValue;
	private float score;
	private bool gameStarted = false;
	private bool inBox = false;

	private float chargeValue=0;
	private float maxCharge=1;
	private float chargeDecay=0.0002f; //Amount decays a second
	private float chargeRatio = 1.5f;
	private GameObject lball = null;
	
	//play with these variables to change the jump 
	private float speed=250;	
	private float minspeed=25; //If it goes below this, speed becomes 0
	private float dragFactor=10;
	private float maxJump = 100; //the jump charge caps at this value.
	private float dragMod = 20;
	private double lballMulti = 2.3;

	private Animator faceAnimator, armsAnimator;
	
	void Start () {
		Transform[] children = GetComponentsInChildren<Transform>();
		foreach(Transform child in children) {
			if (child.name == "face_sprite")
			{
				faceAnimator = child.GetComponent<Animator>();
			}
			else if (child.name == "arms_sprite")
			{
				armsAnimator = child.GetComponent<Animator>();
			}
		}
		if(faceAnimator == null) {
			throw new MissingComponentException("No faceAnimator found in children of KittenController");
		}
		if(armsAnimator == null) {
			throw new MissingComponentException("No armsAnimator found in children of KittenController");
		}
	}
	
	void Update() {	
		edgeValue = Mathf.Min (Screen.width, Screen.height) /3.5f;
		decayCharge();
		checkSpeed ();
		checkJump ();
		points ();
	}

	public void setStart(float x, float y) {
		startX = x;
		startY = y;
	}

	public void startGame() {
		score = 0;
		rigidbody2D.velocity = new Vector2 (0f, 0f);
		gameStarted = true;
	}

	public void endGame() {
		gameStarted = false;
		rigidbody2D.transform.position = new Vector2(startX,startY);
	}
	
	public bool isInBox() {
		return inBox;
	}

	void points() {
		if (inBox)
			score++;
	}

	public string getScore() {
		return score.ToString ();
	}

	void checkSpeed() {
		// Set speed to zero if slow enough
		if (jumping && rigidbody2D.velocity.magnitude<minspeed) {
			rigidbody2D.velocity = new Vector2(0f, 0f);
		}
		//see if jumping
		if (Mathf.Abs(rigidbody2D.velocity.x) <= 1 && 
		    Mathf.Abs(rigidbody2D.velocity.y) <= 1 && jumping) {
			jumping = false;
			// animate idle
			armsAnimator.SetInteger("arms_state", 0);
			faceAnimator.SetInteger("face_state", 0);
			
			destroyLBall();
			if (Time.time - tempTime < lagTime) {
				makeMove(tempHopDir, tempMagnitude);
				tempHopDir = Vector3.zero;
				tempMagnitude = 0;
			}
		}
	}

	void checkJump() {
		// stop if on edge
		if (Mathf.Abs(rigidbody2D.transform.position.x) >= edgeValue) {
			Vector2 newVector;
			if (rigidbody2D.transform.position.x < 0){
				newVector = new Vector2(-1*edgeValue,rigidbody2D.transform.position.y);
			} else{
				newVector = new Vector2(edgeValue,rigidbody2D.transform.position.y);
			}
				rigidbody2D.transform.position = newVector;
		}
		if (Mathf.Abs(rigidbody2D.transform.position.y) >= edgeValue) {
			Vector2 newVector;
			if (rigidbody2D.transform.position.y < 0){
				newVector = new Vector2(rigidbody2D.transform.position.x,-1*edgeValue);
			} else{
				newVector = new Vector2(rigidbody2D.transform.position.x,edgeValue);
			}
			rigidbody2D.transform.position = newVector;
		}
	}

	private float findDrag (){
		if(speedRev == 0) {
			return dragFactor * dragMod;
		} else {
			return (dragFactor*dragMod) / speedRev;
		}
	}
	
	public void remove(){
		Destroy (gameObject);
	}
	
	public void makeMove(Vector3 hopDir, float magnitude){
		if (!jumping) {
			// animate flying
			armsAnimator.SetInteger("arms_state", 2);
			faceAnimator.SetInteger("face_state", 2);
			
			tempHopDir = Vector3.zero;
			tempMagnitude = 0;
			if (lBallExists()){
				speedRev = magnitude * maxJump * (float) lballMulti;
			} else{
				speedRev = magnitude * maxJump;
			}
			Vector2 facingDir = new Vector2 (hopDir.x, hopDir.y);
			rigidbody2D.drag = findDrag (); 
			rigidbody2D.velocity = facingDir * speed * speedRev;
			speedRev = 0;

			// Rotate cat for jumping
			float angle = Mathf.Atan2(hopDir.y, hopDir.x) * Mathf.Rad2Deg;
			angle -= 90;
			transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));

			jumping = true;
		} else {
			tempHopDir = hopDir;
			tempMagnitude = magnitude;
			tempTime = Time.time;
		}
	}

	public void charge(float value){
		chargeValue += (chargeRatio * value);
		if (chargeValue >= maxCharge) {
			chargeValue = maxCharge;
			discharge();
		}
	}

	void decayCharge() {
		chargeValue -= chargeDecay*Time.deltaTime;
		if (chargeValue < 0) {
			chargeValue = 0;
		}
	}

	public void discharge() {
		if (chargeValue == 0) {
			return;
		}
		Object prefab = Resources.Load<GameObject>("ElectricBall");
		lball=Instantiate(prefab,this.transform.position, Quaternion.identity) as GameObject;
		lball.transform.parent=transform;
		chargeValue=0;
	}

	public void hit(float x, float y) {
		tempHopDir = Vector3.zero;
		tempMagnitude = 0;
		if (lBallExists()){
			return;
		} else{
			speedRev = (float) 2 * maxJump;
		}
		float myX = Mathf.Abs (x) - Mathf.Abs (transform.position.x);
		myX = x - transform.position.x;
		float myY = Mathf.Abs (y) - Mathf.Abs (transform.position.y);
		myY = y - transform.position.y;
		rigidbody2D.drag = findDrag (); 
		Vector2 facingDir = new Vector2 (-myX, -myY);
		rigidbody2D.velocity = facingDir * speed * speedRev;
		speedRev = 0;
		jumping = true;
	}

	bool lBallExists() {
		if (lball == null)
			return false;
		return true;
	}

	void destroyLBall() {
		if (lball != null) {
			GameObject.Destroy(lball);
		}
		lball = null;
	}

	void OnTriggerEnter2D(Collider2D collision) {
		if (collision.tag == "pointbox") {
			inBox = true;
		}
	}

	void OnTriggerExit2D(Collider2D collision) {
		if (collision.tag == "pointbox") {
			inBox = false;
		}
	}

	public void setColor(Color color) {
		SpriteRenderer[] r = this.GetComponentsInChildren<SpriteRenderer> ();
		for (int i=0; i<r.Length; i++) {
			// 2 is eyes, 3 is fur
			if (i!=2 && i!=3)
				r[i].material.color=color;
		}
	}

	public Color getColor() {
		SpriteRenderer[] r = this.GetComponentsInChildren<SpriteRenderer> ();
		return r [0].material.color;
	}
}

