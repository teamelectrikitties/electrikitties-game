﻿using UnityEngine;
using System.Collections;

public class PointBox : MonoBehaviour {

	private float timeLastMove;
	private InputController input;
	private float startX;
	private float startY;

	// Use this for initialization
	void Start () {
		startX = rigidbody2D.transform.position.x;
		startY = rigidbody2D.transform.position.y;
		input = InputController.Singleton;
		timeLastMove = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if (input.hasStarted ()) {
			if (Time.time - timeLastMove >= 6) {
					timeLastMove = Time.time;
					rigidbody2D.transform.position = new Vector2 (Random.Range (-60, 60), Random.Range (-60, 60));
			} 
		}
	}

	public void ResetPosition() {
		rigidbody2D.transform.position = new Vector2 (startX, startY);
	}
}
