﻿using UnityEngine;
using System.Collections;
using System;

public class ControllerClient : MonoBehaviour {

	public static ControllerClient Singleton;

	private string ipStr = "127.0.0.1";
	private string portStr = "3030";

	bool isConnected = false;

	// Use this for initialization
	void Start () {
		if(Singleton == null) {
			Singleton = this;
		} else {
			throw new Exception("Multiple ControllerClients!!!");
		}
	}
	
	void OnGUI () {
		if(!isConnected) {
			float widthUnit = Screen.width / 30f;
			float heightUnit = Screen.height / 53f;

			GUIStyle buttonStyle = new GUIStyle(GUI.skin.button);
			buttonStyle.fontSize=48;
			GUIStyle textStyle = new GUIStyle(GUI.skin.textField);
			textStyle.fontSize=30;

			ipStr = GUI.TextField(new Rect (widthUnit, heightUnit, widthUnit * 20, heightUnit * 4), ipStr, 25, textStyle);
			portStr = GUI.TextField(new Rect (widthUnit * 22, heightUnit, widthUnit * 7, heightUnit * 4), portStr, 5, textStyle);

			if (GUI.Button (new Rect (widthUnit, heightUnit * 6, widthUnit * 28, heightUnit * 6), "Connect",buttonStyle)) {
				int portNum;
				try {
					portNum = Convert.ToInt32(portStr);
				} catch (Exception) {
					Debug.LogError("Invalid port number");
					return;
				}
				NetworkConnectionError error = Network.Connect(ipStr, portNum);
				Debug.Log("Attempting connection... " + error);
			}
		}
	}

	void OnConnectedToServer() {
		Debug.Log("Connected to server");
		isConnected = true;
	}

	void OnFailedToConnect(NetworkConnectionError error) {
		Debug.Log ("Failed to connect to server: " + error);
	}

	void OnDisconnectedFromServer(NetworkDisconnection info) {
		Debug.Log ("Disconnected from server: " + info);
		isConnected = false;
	}
	
	// Update is called once per frame
	void Update () {
	}

	public void SendCharge(float magnitude) {
		if(magnitude > 1 || magnitude < 0) {
			throw new Exception("Invalid magnitude");
		}

		if(isConnected) {
			networkView.RPC("ReceiveCharge", RPCMode.Server, magnitude);
		}
	}

	public void SendHop(Vector3 hopDir, float magnitude) {
		if(magnitude > 1 || magnitude < 0) {
			throw new Exception("Invalid magnitude");
		}
		hopDir.Normalize();

		if(isConnected) {
			networkView.RPC("ReceiveHop", RPCMode.Server, hopDir, magnitude);
		}
	}

	[RPC]
	void Ping() {
		Debug.Log("Ping " + Time.time);
	}

	[RPC]
	void ReceiveHop(Vector3 hopDir, float magnitude) {
		Debug.Log("ReceiveHop(" + hopDir + ", " + magnitude + ") @" + Time.time);
	}

	[RPC]
	void ReceiveCharge(float magnitude) {
		Debug.Log("ReceiveCharge(" + magnitude + ") @" + Time.time);
	}

	[RPC]
	void ChangeColor(Vector3 color, NetworkMessageInfo info) {
		Debug.Log ("ChangeColor(" + color + ") @" + Time.time);
		GameObject kittenControllerObject = GameObject.FindGameObjectWithTag ("Player");
		KittenControllerBehaviour kittenController = kittenControllerObject.GetComponent<KittenControllerBehaviour> ();
		kittenController.SetColor (new Color(color.x, color.y, color.z));
	}
}
