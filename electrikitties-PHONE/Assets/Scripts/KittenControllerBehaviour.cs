﻿using UnityEngine;
using System.Collections;

public class KittenControllerBehaviour : MonoBehaviour {
	
	public enum KittenControllerState {
		DEFAULT,
		DRAGGING,
		RELEASED,
		RUBBING
	}

	KittenControllerState state = KittenControllerState.DEFAULT;
	float returnSpeed = 100f;
	float rubToChargeRatio = 0.015f;
	float maxChargeBuildupBeforeSend = 0.1f;
	float minTimeBetweenChargeSends = 1.5f;
	float maxTimeBetweenChargeSends = 3f;
	// distance allowed to pull tail
	float maxDist = 3f;

	float timeLastChargeSent;
	Vector3 lastPointTouched = Vector3.zero;
	float builtUpCharge = 0;

	GameObject tail;
	Vector3 tailStartScale;

	GameObject electricBall;
	float minDistRubbedForElectricBall = 0.1f;

    private Animator faceAnimator, armsAnimator;

	// Use this for initialization
	void Start () {
		Transform[] children = GetComponentsInChildren<Transform>();
		foreach(Transform child in children) {
			if(child.name == "tail") {
				tail = child.gameObject;
			}
            else if (child.name == "face_sprite")
            {
                faceAnimator = child.GetComponent<Animator>();
            }
            else if (child.name == "arms_sprite")
            {
				armsAnimator = child.GetComponent<Animator>();
            }
			else if(child.name == "ElectricBall") 
			{
				electricBall = child.gameObject;
			}
		}
		if(tail == null) {
			throw new MissingComponentException("No tail found in children of KittenController");
		}
		if(faceAnimator == null) {
			throw new MissingComponentException("No faceAnimator found in children of KittenController");
		}
		if(armsAnimator == null) {
			throw new MissingComponentException("No armsAnimator found in children of KittenController");
		}
		if(electricBall == null) {
			throw new MissingComponentException("No ElectricBall found in children of KittenController");
		}
		electricBall.renderer.enabled = false;
		tailStartScale = tail.transform.localScale;
		timeLastChargeSent = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
		bool isTouchDown = false;
		Vector3 touchCoords = lastPointTouched;
		bool isTouchingCat = false;
		if(IsTouchDown()) {
			isTouchDown = true;
			touchCoords = GetTouchCoords();
			isTouchingCat = IsTouchingCat();
		}

		float timeSinceLastChargeSent = Time.time - timeLastChargeSent;
		bool hasChargedEnough = builtUpCharge > maxChargeBuildupBeforeSend && timeSinceLastChargeSent > minTimeBetweenChargeSends;
		bool mustSend = builtUpCharge > 0 && timeSinceLastChargeSent > maxTimeBetweenChargeSends;
		if (hasChargedEnough || mustSend) {
			ControllerClient.Singleton.SendCharge(builtUpCharge);
			builtUpCharge = 0;
			timeLastChargeSent = Time.time;
		}

		switch (state) {
		case KittenControllerState.DEFAULT: {
			if(isTouchDown) {
				if(isTouchingCat) {
					state = KittenControllerState.DRAGGING;
				} else {
					state = KittenControllerState.RUBBING;
				}
			}
			break;
		}
		case KittenControllerState.DRAGGING: {
			if(!isTouchDown) {
				state = KittenControllerState.RELEASED;
				
				Vector3 pulledVector = lastPointTouched - tail.transform.position;
				float pullMagnitude = Mathf.Min(pulledVector.magnitude + 1, maxDist);
				Vector3 hopDir = -pulledVector.normalized;
				ControllerClient.Singleton.SendHop(hopDir, pullMagnitude / maxDist);
				
				tail.transform.localScale = tailStartScale;
			} else {
				armsAnimator.SetInteger("arms_state", 1);
				faceAnimator.SetInteger("face_state", 1);
				Vector3 pulledVector = touchCoords - tail.transform.position;
				float pullMagnitude = Mathf.Min(pulledVector.magnitude + 1, maxDist);

				ScaleTail(pullMagnitude);

				float angle = Vector3.Angle(Vector3.down, touchCoords);
				// .Angle returns the closest acute angle, need to negate the angle
				// if the x coord is negative to allow the cat to fully rotate
				if(touchCoords.x < 0) {
					angle = -angle;
				}

				rotateCat(angle);
			}

			break;
		}
		case KittenControllerState.RELEASED: {
			armsAnimator.SetInteger("arms_state", 0);
			faceAnimator.SetInteger("face_state", 0);

			float step = Time.deltaTime * returnSpeed;
			
            // move the tail back to the idle scale
			tail.transform.localScale = Vector3.MoveTowards(tail.transform.localScale, tailStartScale, step);
			if(tail.transform.localScale == tailStartScale) {
				state = KittenControllerState.DEFAULT;
			}
			break;
		}
		case KittenControllerState.RUBBING: {
			if(!IsTouchDown()) {
				electricBall.renderer.enabled = false;
				state = KittenControllerState.DEFAULT;
			} else {
				if(lastPointTouched != Vector3.zero) {
					float distRubbed = Mathf.Abs(Vector3.Distance(touchCoords, lastPointTouched));
					if(distRubbed >= minDistRubbedForElectricBall) {
						electricBall.renderer.enabled = true;
						electricBall.transform.position = touchCoords;
					} else {
						electricBall.renderer.enabled = false;
					}

					builtUpCharge += distRubbed * rubToChargeRatio;
					if(builtUpCharge > 1) {
						builtUpCharge = 1;
					}
					lastPointTouched = touchCoords;
				}
			}
			break;
		}
		default:
			break;
		}
		if(isTouchDown) {
			lastPointTouched = touchCoords;
		}
	}

	private Vector3 GetTouchCoords() {
		Vector3 mousePos = Input.mousePosition;
		mousePos.z = Camera.main.transform.position.z;
		Vector3 worldPoint = Camera.main.ScreenToWorldPoint(mousePos);
		worldPoint.z = 0;
		return worldPoint;
	}

	private bool IsTouchingCat() {
		if (!IsTouchDown ()) {
			return false;
		}
		Vector3 touchCoords = GetTouchCoords ();
		Vector2 touchCoords2D = new Vector2 (touchCoords.x, touchCoords.y);
		Collider2D tailCollider2D = tail.GetComponentInChildren<Collider2D>();
		return tailCollider2D.OverlapPoint (touchCoords2D);
	}

	private bool IsTouchDown() {
		return Input.GetMouseButton(0);
	}

	private void ScaleTail(float magnitude) {
		tail.transform.localScale = new Vector3(1, magnitude, 1);
	}

	private void rotateCat(float angle) {
        // rotate the cat to rotate all the children gameObjects with it
		transform.eulerAngles = new Vector3 (0, 0, angle);
	}

	public void SetColor(Color color) {
		Transform[] children = GetComponentsInChildren<Transform>();
		foreach(Transform child in children) {
			if(child.name != "face_sprite" && child.name != "ElectricBall") {
				if(child.renderer != null) {
					child.renderer.material.color = color;
				}
			}
		}
	}
}
